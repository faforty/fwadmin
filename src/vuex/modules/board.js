import {
    RECEIVE_POSTS,
    BOARD_ADD_CAT
} from '../mutation-types'
import * as api from '../../api/'

const state = {
    posts: [],
    cats: []
}


const mutations = {
    [BOARD_ADD_CAT] (state, cat) {
        state.cats.push(cat)

        console.log(state.cats)
    },
    [RECEIVE_POSTS] (state, post) {
        state.posts.push(post)
    }
}

const actions = {
    createCat({commit}, cat) {
        commit(BOARD_ADD_CAT, cat)
    }
}

const getters = {
    getBoardCats: state => state.cats
}

export default {
    state,
    actions,
    getters,
    mutations
}